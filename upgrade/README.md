# RAM microservices upgrade

## WHY

Once the intial deployment complete successfully, it is important to have a defined process to upgrade your RAM instance when a new RAM version is released, so that security fixes, bug fixes, feature improvements and new features are deployed to production, so that the solution continue delivering value.

## WHAT

As RAM is deliverd through a set of Docker container images, this operation is done by:

1. Deploying a new revision of each Cloud Run service using the targeted release version AND with no traffic
2. Checking deployment status is sucessfull, aka the logs shows Ready condition set to true.
3. Split traffic (hydrate) to the new revision using an adapted progressivity, for example
   - in produciton: 1%, 10%, 50%, 100%
   - in test directly 100%
4. Check execution status is sucessful for the new version, aka not errors in logs

In case the new version brings issuesm just flip traffic to 0% on this revision that will send traffic back the traffic to the previous revision, performing so [a fast roll-back](https://cloud.google.com/run/docs/rollouts-rollbacks-traffic-migration).

Cloud Run number of revision limit is 1000, AND it is a good practice to delete obsolete revisions to keep the system clear.

## HOW

Multiple options are available to automate upgrade steps A & C, by:

- running a **shell script** to execute glcoud run deploy commands sequentially from your computer
- submiting **Google_Cloud_Build builds** that execute the same in parallel, from the Cloud Build worker, with logs
- having **one comprehensive GitLab CI pipeline** with a deploy stage and an hydrate stage per enviroments, executing microservices deployments in parallel, from the GitLab workers, with logs and a comprehensive pipeline view.
- ...

First set environment variables:

- Deploy service account means `ram-deploy` service account provisioned by Terraform during the previous step
- Environment means test, prod ... (dev environment is already used to host code and docker container image repo. so please do not use it for another meaning)
- This can be a `set_env_vars.sh` file to be run with `source ./set_env_vars.sh`

```shell
# $1 is the environment, test or prod. test by default. do not use dev that is reserved for the project hosting docker container images, code unit and integration tests
if [[ -z "$1" ]]; then
    environment="test"
else
    environment=$1
fi
if [ $environment == "test" ] || [ $environment == "prod" ]; then
    if [ $environment == "prod" ]; then
        export GOOGLE_APPLICATION_CREDENTIALS="/your/path/to/deploy_service_account_key-production.json"
        export PROJECT_ID="your-gcp-project-id-production"
        export ENVIRONMENT="prod"
    else
        export GOOGLE_APPLICATION_CREDENTIALS="/your/path/to/deploy_service_account_key-test.json"
        export PROJECT_ID="your-gcp-project-id-test"
        export ENVIRONMENT="test"
    fi
    echo PROJECT_ID $PROJECT_ID
    echo  ENVIRONMENT $ENVIRONMENT
else
    echo "Error - environment must be 'test' or 'prod'"
fi
```

### Upgrade using a shell script

- Review the following scripts and COMPLEMENT environment variable when needed
- To deploy zero traffic:
  - `./deploy_no_traffic.sh ram-v0-2-0`
- To hydrate (100 means 100%):
  - `./update_traffic.sh ram-v0-2-0 100`

### Upgrade using Google_Cloud_Build builds

- Review the following `yaml` configuration files and COMPLEMENT environment variable when needed
- Clear Cloud Build [pre-requisites](https://cloud.google.com/build/docs/deploying-builds/deploy-cloud-run#required_iam_permissions) for example using the script `iam_bindings_cloud_build.sh`
- To deploy zero traffic:
  - `gcloud builds submit --no-source --async --config=deploy_no_traffic.yaml --substitutions=_VERSION=latest,_ENVIRONMENT=$ENVIRONMENT,_PROJECT_ID=$PROJECT_ID --project=$PROJECT_ID`
  - `--async` means go to Cloud Build history to check progress
  - When `_VERSION` substitution is omitted the default value is `latest`
- To hydrate::
  - `gcloud builds submit --no-source --async --config=update_traffic.yaml --substitutions=_TAG=latest,_TRAFFIC_PERCENT=100,_PROJECT_ID=$PROJECT_ID --project=$PROJECT_ID`
  - When `_TAG` substitution is omitted the default value is `latest`
  - When `_TRAFFIC_PERCENT` substitution is omitted the default value is `100`

### Upgrade using GitLab CI

A example is provided in the core repository: [.gitlab-ci.yml](https://gitlab.com/realtime-asset-monitor/core/-/blob/main/.gitlab-ci.yml)  
This example is working for 4 core microservices, with basic settings, using the gitlab [parallel:matix](https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix) statement.  
Adapting this template to all RAM microservices, with similar setting as for the `gcloud` or `cloud build` above did not work due to a limitation on the string size for `ENV_VARS` fields.
