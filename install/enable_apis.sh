#!/usr/bin/env bash

# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the 'License');
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an 'AS IS' BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

gcloud config set project $PROJECT_ID

echo enable APIs $PROJECT_ID
gcloud services enable\
    artifactregistry.googleapis.com\
    bigquery.googleapis.com\
    cloudasset.googleapis.com\
    cloudbuild.googleapis.com\
    cloudprofiler.googleapis.com\
    cloudscheduler.googleapis.com\
    cloudtrace.googleapis.com\
    compute.googleapis.com\
    eventarc.googleapis.com\
    firestore.googleapis.com\
    iap.googleapis.com\
    logging.googleapis.com\
    monitoring.googleapis.com\
    pubsub.googleapis.com\
    run.googleapis.com\
    storage.googleapis.com\
    --project=$PROJECT_ID
