# WARNING - Evaluating GKE pod changes in real-time lead to significant extra cost

By design Kubernetes is continuously changing the pods configuration. This may represent a couple of thousand change per second for a full organization, resulting in ~ billion asset changes a month.

The rules in this folder are usefull and they should be used ONLY with RAM batch mode.

DO: schedule `k8s.io/Pod` asset type export every day of every 3 hours, using the `actions.json` setting file.

DO NOT: add `k8s.io/Pod` asset type to the terraform Cloud Asset Inventory real-time feed configuration.

## Know issue

There is a side effect NOT to monitor `k8s.io/Pod` assets changes in real-time: RAM will never get the delete feed message. Which means, if a `k8s.io/Pod` is not compliant and then deleted, this asset violations will stay active in RAM reports, leading to false positves as the asset has be deleted.
