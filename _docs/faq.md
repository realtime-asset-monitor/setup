# FAQ

## RAMv2 vs RAMv1

### How much does RAMv2 reduce costs compared to RAMv1?

**10x**.  
The cost reduction comes from the following combination of improvements:

- The code of each microservices has been refactored to leverage Cloud Run multi-concurrency, so that the number of Cloud Run instances used to deal with a full export of GCP assets is far less than the number of Cloud Functions v1 instances used in RAMv1.
- There are 10 Cloud Run services in RAMv2 compared to +200 Cloud Functions for a typical RAMv1 setup, leading to better resource sharing. Moreover the memory and CPU settings have been tuned for each Cloud Run service.
- The logging level is configurable, so that `INFO` log entry that fully records every triggering event could be activated only in `dev` and `qa` environments, and not in `test` and `prod` environments, leading `cloud Logging - Log volume` NOT to be the top one run cost.
- `convertfeed` microservices uses a in memory cache managing multi-concurrency for read and write operations to resolve assets ancestors reference like `organization/1234567890123` to friendly ancestor names like `myorg.com`. Moreover, resolving ancestor reference to ancestor friendly name is done once in `convertfeed` at the beginning of the event flow instead of 3 times in RAMv1 (monitor, publish2fs and upload2gcs)
- The ack_deadline parameter of the triggering Pub/Sub push subscriptions have been tuned to avoid useless retry meaning duplicate processing during peak load when exporting all GCP assets, leading to less `Cloud Run - CPU Allocation Time`, less `Cloud Run - Memory Allocation Time` less `BigQuery - Streaming insert`, less `BigQuery - Active Storage`, and less BigQuery best effort deduplication works.
- The filter parameter of the triggering Pub/Sub push subscriptions have been set so that:
  - `publish2fs` is triggered only to process resources manager assets (aka org / folders / projects) instead of all assets.
  - `upload2gcs` is triggered only to process the asset type you are developing custom rules on instead of all of them.
- BigQuery:
  - By default, the views are reading the tables on a 7 days interval (consistent with a weekly export period) which optimize job cost.
  - By default, the table partitions expire after 28 days.

### How is RAMv2 more open than RAMv1?

- Open to any kind of assets, meaning not only GCP.
  - RAMv2 uses a unique event pipe to process all types of assets instead of one pipe per asset type like RAMv1, so that integrating  a new asset flow does not require any change in RAMv2 deployment. Just publish messages in caiFeed topic, and add the related rules in the ruleRepo: No resources deployment, No RAM settings changes. This is visible in [RAMv2 graph](ram.svg)
- Open to the CICD of your choice
  - Unlike RAMv1, RAMv2 does not require Cloud Build as the deployment tool. RAMv2 microservices are shared as Docker container images leveraging a Container as a Service approach CaaS. The source Go code is still available in open source mode like RAMv1, the CI chain too, if you want to rebuild images from source code, but there's no need to do so. Just deploy from the public Google Artifact Registry sharing the Docker images.
    - RAMv2 infrastructure (Cloud Run services, triggers, topics, tables, services accounts, iam etc..) is deployed using a [Terraform module](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor) to be executed in your own terraform CICD pipe.
    - RAM microservice upgrade can be performed using a simple script or the CICd pipe of your choice.
- Open to the execution environment of your choice
  - By default RAMv2 is deployed on Cloud Run leveraging the serverless promise to pay only when used, meaning truly scaling to zero when not used. That said, as any set of Docker container images, it can be deployed in other run platforms, like Kubernetes based service for example GKE.
- RAMv2 uses the [Function Framework](https://cloud.google.com/functions/docs/functions-framework) to write lightweight functions that run in many different environments.
- RAMv2 uses [Cloud Event format](https://cloudevents.io/) to specify event data in a common way, which does not mean EventArc.

### How is RAMv2 more flexible than RAMv1?

- Scope assets to be scanned by any combination of GCP folders and or organizations, meaning not only a list of organizations like with RAMv1.
  - This applies both when configuring the Cloud Asset Inventory Feeds and Cloud Asset Inventory Exports.
- Organize your own rule repository by naming files and folders as you wish, so that the directory and file structure is no longer imposed as with RAMv1.
  - More information:  [How to setup your rule repo](../test/README.md)
- Cloud Run deployment enables deployment of a new service revision with no traffic, progressively shifting the traffic to the new revision, or shifting the traffic back to a previous revision. Enabling fast rollback and progressive roll out.

### How is RAMv2 more simple than RAMv1?

- 10 Cloud Run services in RAMv2 vs +200 cloud functions in RAMv1
- 8 Pub/Sub topics in RAMv2 vs ~50 topics in RAMv1
- Effective separation between application code and config and rules code in RAMv2 vs mixed application code and rule config in RAMv1 Cloud Function files.
- Each cloud function deployment in RAMv1 recompiles the RAM cli in Cloud Build vs deploy from Docker images in RAMv2
- Infra provisioning based on Terraform enables resources deletion / destruction in RAMv2 vs no resources delete in RAMv2 cli.

### How does RAMv2 implement more security and quality good practices than RAMv1?

- Code refactored and better testing:
  - RAMv2 is ~9.8k go code lines including test code at +80% coverage
    - Testing includes unit tests and integration tests. Integration tests consume the GCP APIs in the dev environment, meaning there is no mocking.
  - RAMv1 is ~18,6k go code lines includung test code at only ~18% coverage
    - The RAM cli in RAMv1 has been fully replaced by a terraform module 8,7k HCL code
    - Test code in RAMv1 only related to RAM cli while it covers all code in RAMv2
- RAMv2 uses the last version of go language vs RAMv1 go version is constrained to go 1.13 by the Cloud Function v1 platform.
- RAMv2 Continuous Integration pipelines implements:
  - All artifact are under version control: go code, test data, docker images, terraform code, setup samples including rule examples
  - Loosely-coupled architecture, resulting for example in one git repo per microservice.
  - Automated go dependencies upgrade
  - SATS Static Application Security Testing
  - Continuous unit and integration testing
  - Local pre-commit checks
  - Docker image creation using the [Cloud Native buildpacks](https://buildpacks.io/)
  - Docker image vulnerability scan using Google Artifact Registry
- The RAMv2 Terraform module manages resource location settings including the Pub/Sub allowed regions.
- Custom Cloud Monitoring dashboards enabling fast error detection and resource setting tuning.

## Architecture

### Why RAMv2 is not using eventarc triggers?

Initially all RAM triggers were EventArc Pub/Sub triggers. An EventArc Pub/Sub trigger creates under the hood a Pub/Sub subscription that triggers the Cloud Run service target. Unfortunately the settings of the underlying Pub/Sub subcription are not exposed in the EventArc settings, which means are set to default values, or not used, while it is required to:

- Set the `Acknowledgement deadline` to a longer value than the 10 sec by default, accordingly to the timeout setting of the related Cloud Run service. Without a consistent setting `splitdump` detect a high rate of duplicate triggering events as an example of related issues.
- Set filters on the cloud event metadata (that are also the pubsub message metadata) to only trigger the Cloud run target when needed, for example `publish2fs` only persit to firestore the resource manager assets (org / folder / project) and should ignore all other asset types. Using a subscription filter enable to reduce cloud run cost.

Furthermore, RAMv2 Infra is provisioned using Infra as Code (aka terraform) which means that the resources status is described in the terraform state and should NOT be updated out side of terraform. So changing the Pub/Sub subcription setting underlying an EventArc trigger out side of terraform is not an option.

At the end of the day the cons using EventArc in the context of RAMv2 weigth more that the pros, explaining why most of EventArc trigger are to be replaced by Pubsub subscriptions. The [RAM graph](ram.svg) is showing Pub/Sub push subscription triggers in dark blue and reamining EventArc triggers in light blue.

### Is RAMv2 using Cloud Event format?

Yes. All microservices, except `splitdump` by design, output events using the Cloud Event format, published to a Pub/Sub topic. The client library automatically set the Pub/Sub message attributes equal to the Cloud Event attributes, which they ease filtering when needed.

## Configuration

### What are the default settings?

RAM microservices settings are defined in environment variables. For each microservice the file `types.go` defines the environment variables in a structure named `ServiceEnv`. For [example](https://gitlab.com/realtime-asset-monitor/fetchrules/-/blob/main/types.go#L34) the `cache_max_age_minutes` in `fetchrules` is set by default to 60. To set a different value, declare the environment variable, in upper case, with a prefix equal to the name of the microservice and an underscore in the cloud run deployent configuration: [example](https://gitlab.com/realtime-asset-monitor/fetchrules/-/blob/main/.gitlab-ci.yml#L34).
