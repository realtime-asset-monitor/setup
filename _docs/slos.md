# Service Level Objectives

## RAM SLOs

**Critical User Journey:** As RAM user, I expect each asset change and asset export to be analyzed successfully (availability) and quickly (latency)

**SLI specification and implentation:** as code in the terraform repo [/modules/slos](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/blob/main/modules/slos/main.tf)

**SLO specification and implementation:** as code. with these [default values](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/blob/main/variables.tf)

### RAM Latency SLO rationales & trade off

- About latency goals:
  - The values may be adapted depending on the type of asset feeds subscribed and type of asset exported.
  - A simple approach is to observe the achievable SLOs in your environment and set the goal accordingly
  - The default goals are conservative, and may be changed to:
    - Latency Schedule threshold may be lowered from 31min to 15.5min, keeping the 99% goal
  - Latency SLOs are mainly driven by Cloud Asset Inventory dependency.
  - The default proposed goals are the same for CAI and RAM as with these goals RAM overhead is negligible
- About the dashboard `slo_1_ram_e2e_latency_scheduled`
  - About the widget `Trigger 2 star...`
    - Using `convertfeed` log entry as a point where to measure is convienient as this microservice logs the `assetType`:
      - These two field are labels in the log based metric t2s_latency
      - Enabling the breakdown the trigger to start latency by assetType / ruleName.
    - This approach works well for the real-time flow while it is less good for the sheduled export flow:
      - Real-time flow:
        - CAI Feed is the step triggering `convertfeed` making this indicator a direct measure of CAI Feed latency
      - Schedule exports flow:
        - Delivered CAI export once are crunched by slitexport (slit to child / split to lines)
        - Delivered lines trigger `convertfeed`
        - So, this indicator is not a direct measure of CAI Export latency as it include `splitexport` overhead
        - To get a direct measurement of CAI export latecny go to dashboard `slo_2_dependency_cai_scheduled`. It as no breakdown by `assetType`

### RAM availability SLO rationales & tradeoff

- About availability goals:
  - Cloud monitoring [API limit the maximum goal to 3 nines (0.999, aka 99.9%)](https://cloud.google.com/monitoring/api/ref_v3/rest/v3/services.serviceLevelObjectives)
  - So these SLOs are set to 99.9% while the achiveable target is above
- There are 8 availability SLOs: one per RAM critical microservice
- Finsh log entries are the good events
- NoRetry log entries are he bad events
- Retry log entries are invalid events as each microservice is idempotent

## RAM dependencies

RAM has two main dependencies:

- The request-responses on Google Cloud APIs
- The data pipelines from Cloud Asset Inventory

Measuring RAM main depencues as SLOs easy the triage effort when RAM SLOs are not healhy.

## RAM depencency on Google APIs

The top 3 APIs consumed by RAM have been identified using the report `Consumed API - Request count [SUM] over the last 28 days`. They are:

1. `google.cloud.bigquery.v2.TableDataService.InsertAll`
2. `google.pubsub.v1.Publisher.Publish`
3. `google.firestore.v1.Firestore.Commit`

**Critical User Journey:** As RAM DevOps eng, I expect critical consumed Google APIs to respond successfully (availability) in a timely maner (latency)

**SLI specification and implentation:** as code in the terraform repo [/modules/transparentslis](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/blob/main/modules/transparentslis/main.tf)

**SLO specification and implementation:** as code. with these [default values](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/blob/main/variables.tf)

### RAM depencency on Google APIs SLO rationales & tradeoff

- Implemented leveraging [Google transparent SLIs](https://cloud.google.com/blog/products/gcp/transparent-slis-see-google-cloud-the-way-your-application-experiences-it)
- Google shares SLIs, do NOT shares SLOs. So SLOs are set using the [achievable approach](https://cloud.google.com/blog/products/management-tools/practical-guide-to-setting-slos). You may need to tune the goals in your context, especially the latency thresholds.

## RAM dependencies on Cloud Asset Inventory data pipelines

- Implemented from RAM, as transparent SLIs exposes only Request-Response APIs SLIs, nothing about data pipelines.
- Where to measure?
  - For the real-time flow based on CAI feeds:
    - Measuring from `convertfeed` microservice as it is directly triggered  by the CAI published Pub/Sub messages
    - Using the t2s_latency log based metric:
      - Filtering on label origin = real-time to discard message from splitexport and keep only messages from CAI Feeds.
      - The triggering event timestamp is the time when the change was done on the asset, which is a fair starting point for real-time flow
  - For the scheduled flow based on CAI exports:
    - Measuring from `splitexport` microservice as it is directly triggered by Cloud Storage when an export object is delivery by CAI
      - WARNING - Bias:
        - when an export is above the configured max number of lines, splitexport first split it in smaller child exports.
        - So the measuremet included also these extra datapoints which
          - over poderate each original CAI export
          - integrate split2child processing time
        - So far observed scheduled flow latency is ~15min for 95th percentile. So it does not justify to invest more to remove this bias
    - Using the t2s_latency log based metric:
      - The triggering event timestamp is the time when the Cloud scheduler starts to request the asset export to CAI, which is a fair starting point for Scheduled flow
    - No breakdowm by `assetType`:
      - CAI export are requested using a [URI prefix](https://cloud.google.com/asset-inventory/docs/reference/rest/v1/TopLevel/exportAssets#gcsdestination) resulting in exports to be delired by shards. The sharding criteria used the `assetType` and expose it in the storage object name.
      - So it main be technically possible to label the t2s_latency metric by `assetType` in the future
