// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"cloud.google.com/go/firestore"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
)

func TestAddExpireAtWhenMissing(t *testing.T) {
	paths := []string{
		"testdata/asset_01.json",
		"testdata/asset_04.json",
		"testdata/asset_05.json",
		"testdata/asset_06.json",
		"testdata/asset_07.json",
		"testdata/asset_08.json",
		"testdata/asset_09.json",
		"testdata/asset_10.json",
	}
	testCases := []struct {
		name            string
		pageSize        int
		wantMsgContains []string
	}{
		{
			name:     "pageSizeLesThanCollectionSize",
			pageSize: 2,
			wantMsgContains: []string{
				"added expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-1",
				"already have expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-2",
				"added expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-3",
				"already have expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-4",
				"added expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-5",
				"already have expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-6",
			},
		},
		{
			name:     "pageSizeLesThanCollectionSize",
			pageSize: 1000,
			wantMsgContains: []string{
				"added expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-1",
				"already have expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-2",
				"added expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-3",
				"already have expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-4",
				"added expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-5",
				"already have expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-6",
			},
		},
	}
	var env struct {
		AssetCollectionID       string `envconfig:"asset_collection_id" default:"assets"`
		AssetRetentionExemption string `envconfig:"asset_retention_exemption" default:"cloudresourcemanager.googleapis.com/"`
		ProjectID               string `envconfig:"project_id" required:"true"`
	}
	err := envconfig.Process(microserviceName, &env)
	if err != nil {
		log.Fatalln(err)
	}

	var ctx = context.Background()
	firestoreClient, err := firestore.NewClient(ctx, env.ProjectID)
	if err != nil {
		log.Fatalln(err)
	}

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			for _, path := range paths {
				p := filepath.Clean(path)
				if !strings.HasPrefix(p, "testdata/") {
					panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
				}
				b, err := os.ReadFile(p)
				if err != nil {
					log.Fatalln(err)
				}
				var feedMessage cai.FeedMessageFS
				err = json.Unmarshal(b, &feedMessage)
				if err != nil {
					log.Fatalln(err)
				}
				documentID := strings.ReplaceAll(feedMessage.Asset.Name, "/", "\\")
				if feedMessage.ContentType != "RESOURCE" && feedMessage.ContentType != "" {
					documentID = fmt.Sprintf("%s_%s", documentID, feedMessage.ContentType)
				}
				documentPath := env.AssetCollectionID + "/" + documentID
				_, err = firestoreClient.Doc(documentPath).Set(ctx, feedMessage)
				if err != nil {
					log.Fatalln(err)
				}
			}
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			err := addExpireAtWhenMissing(tc.pageSize)
			msgString := buffer.String()
			// t.Logf("\n%v\n", msgString)
			if err != nil {
				log.Fatalln(err)
			}
			for _, wantMsg := range tc.wantMsgContains {
				if !strings.Contains(msgString, wantMsg) {
					t.Errorf("want msg to contains: %s", wantMsg)
				}
			}
		})
	}
}
