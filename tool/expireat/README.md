# expireat tool

## Issue

Before v0.9.0 firestore doc in asset collection did not have a `expireAt` field.

So even if v0.9.0 is deployed, these doc will never be deleted by the TTL policy

## Solution

This tool:

- list all docs in the assets collection
- If `asset.assetType` does not contains "resourcemanager" and `expireAt` is not set
  - then, add `expireAt` with a value set to today

The active TTL policy will remove the document within 24h.

## How to use

- create a `set_env_vars.sh` script file with a content like:

```shell
#!/usr/bin/env bash

# to be run as source ensuring env vars to be available after the script completes  
# source ./set_env_vars.sh

export PUBLISH2FS_PROJECT_ID="<your-project-id>>"

export GOOGLE_APPLICATION_CREDENTIALS="<your-json-key-file"
```

- `source ./set_env_vars.sh`
- copy the executable `expireat`
- The tool output may be a lot of lines it look like:

```shell
2024/07/10 16:40:21 exempted asset: //cloudresourcemanager.googleapis.com/projects/1234567890123
2024/07/10 16:40:21 exempted asset: //cloudresourcemanager.googleapis.com/projects/0987654321235
2024/07/10 16:40:21 added expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-1
2024/07/10 16:40:21 already have expireAt: //container.googleapis.com/projects/qwertyu-ram-noncompliant/zones/us-central1-c/clusters/cluster-2
```

- run `./expireat 2> results.txt`
