// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"cloud.google.com/go/firestore"
	"github.com/kelseyhightower/envconfig"
)

const microserviceName = "publish2fs"

func main() {
	err := addExpireAtWhenMissing(1000)
	if err != nil {
		log.Fatalf("%v\n", err)
	}
}

func addExpireAtWhenMissing(pageSize int) (err error) {
	var env struct {
		AssetCollectionID       string `envconfig:"asset_collection_id" default:"assets"`
		AssetRetentionExemption string `envconfig:"asset_retention_exemption" default:"cloudresourcemanager.googleapis.com/"`
		ProjectID               string `envconfig:"project_id" required:"true"`
	}
	err = envconfig.Process(microserviceName, &env)
	if err != nil {
		return err
	}

	var ctx = context.Background()
	firestoreClient, err := firestore.NewClient(ctx, env.ProjectID)
	if err != nil {
		return err
	}

	myQuery := firestoreClient.Collection(env.AssetCollectionID).
		Where("asset.assetType", "not-in", []string{"cloudresourcemanager.googleapis.com/Project", "cloudresourcemanager.googleapis.com/Folder", "cloudresourcemanager.googleapis.com/Organization"})

	firstPage := myQuery.
		Limit(pageSize).
		Documents(ctx)

	docs, err := firstPage.GetAll()
	if err != nil {
		return err
	}
	for {
		if len(docs) > 0 {
			for _, doc := range docs {
				// log.Println(doc.Ref.Path)
				assetMap := doc.Data()
				var assetInterface interface{} = assetMap["asset"]
				if asset, ok := assetInterface.(map[string]interface{}); ok {
					var nameInterface interface{} = asset["name"]
					if name, ok := nameInterface.(string); ok {
						var assetTypeInterface interface{} = asset["assetType"]
						if assetType, ok := assetTypeInterface.(string); ok {
							if !strings.Contains(assetType, env.AssetRetentionExemption) {
								if _, ok := doc.Data()["expireAt"]; !ok {
									_, err := doc.Ref.Update(ctx, []firestore.Update{
										{
											Path:  "expireAt",
											Value: time.Now(), // This will be stored as a Firestore timestamp
										},
									})
									if err != nil {
										return fmt.Errorf("failed to update document: %s %v", doc.Ref.Path, err)
									}
									log.Printf("added expireAt: %s", name)
								} else {
									log.Printf("already have expireAt: %s", name)
								}
							} else {
								log.Printf("exempted asset: %s", name)
							}
						} else {
							log.Printf("missing assetType: %s", name)
						}
					} else {
						log.Printf("missing assetName: %s", doc.Ref.Path)
					}
				} else {
					log.Printf("not an asset doc: %s", doc.Ref.Path)
				}
			}
			lastDoc := docs[len(docs)-1]
			myQuery = myQuery.StartAfter(lastDoc)
			page := myQuery.
				Limit(pageSize).
				Documents(ctx)

			docs, err = page.GetAll()
			if err != nil {
				return err
			}
		} else {
			break
		}
	}
	return nil
}
