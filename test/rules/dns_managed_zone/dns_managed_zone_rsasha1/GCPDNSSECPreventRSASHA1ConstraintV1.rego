# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
package templates.gcp.GCPDNSSECPreventRSASHA1ConstraintV1

import data.validator.gcp.lib as lib
import future.keywords.in

deny[{
	"msg": message,
	"details": metadata,
}] {
	constraint := input.constraint
	lib.get_constraint_params(constraint, params)

	asset := input.asset
	asset.asset_type == "dns.googleapis.com/ManagedZone"

	dnssecConfig := asset.resource.data.dnssecConfig

	forbidden_algos := lib.get_default(params, "forbidden_algos", ["RSASHA1"])
	trace(sprintf("forbidden_algos %v", [forbidden_algos]))
	
	matching_algos := [ keySpec.algorithm | keySpec := dnssecConfig.defaultKeySpecs[_]; keySpec.algorithm in forbidden_algos ]
	trace(sprintf("matching_algos %v", [matching_algos]))
	count(matching_algos) != 0


	message := sprintf("%v: DNSSEC has forbidden algorithm enabled", [asset.name])
	metadata := {"resource": asset.name}
}

