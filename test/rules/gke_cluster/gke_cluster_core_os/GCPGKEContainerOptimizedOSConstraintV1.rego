# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#Check is cluster is using CoreOs or Not
package templates.gcp.GCPGKEContainerOptimizedOSConstraintV1

import data.validator.gcp.lib as lib
import future.keywords.in

deny[{
    "msg": message,
    "details": metadata,
}] {
    constraint := input.constraint
    lib.get_constraint_params(constraint, params)

	mode := lib.get_default(params, "mode", "allowlist")
	
    # trace(sprintf("mode : %v", [mode]))
    images := lib.get_default(params, "image", ["COS"])
    # trace(sprintf("images : %v", [images]))

    asset := input.asset
    asset.asset_type == "container.googleapis.com/Cluster"
    cluster := asset.resource.data
    node_pools := lib.get_default(cluster, "nodePools", [])
    node_pool := node_pools[_]
    check_image(node_pool,images,mode)

    message := sprintf("Cluster %v has node pool %v without Container-Optimized OS.", [asset.name, node_pool.name])
    metadata := {"resource": asset.name}
}

###########################
# Rule Utilities
###########################
check_image(node_pool,images,mode) {

    nodeConfig := lib.get_default(node_pool, "config", {})
    imageType := lib.get_default(nodeConfig, "imageType", "")
    trace(sprintf("imageType : %v", [imageType]))
    mode == "denylist"
    imageType in images
}

check_image(node_pool,images,mode) {

    nodeConfig := lib.get_default(node_pool, "config", {})
    imageType := lib.get_default(nodeConfig, "imageType", "")
    trace(sprintf("imageType : %v", [imageType]))
    mode == "allowlist"
    not imageType in images
}