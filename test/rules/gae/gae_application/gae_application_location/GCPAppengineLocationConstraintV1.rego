# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
package templates.gcp.GCPAppengineLocationConstraintV1

import data.validator.gcp.lib as lib
import future.keywords.in

deny[{
	"msg": message,
	"details": metadata,
}] {
	constraint := input.constraint
	lib.get_constraint_params(constraint, params)

	asset := input.asset

	# Applies to appengine applications only
	asset.asset_type == "appengine.googleapis.com/Application"

	# Retrieve the list of allowed locations
    mode := lib.get_default(params, "mode", "allowlist") 
	target_locations := lib.get_default(params, "locations", ["EU"])

    asset_location := asset.resource.data.locationId
	trace(sprintf("mode: %v, asset_location:%v, target_locations:%v", [mode,asset_location,target_locations]))
	
	check_location(mode,asset_location,target_locations)

	message := sprintf("Application %v is in a disallowed location (%v).", [asset.name,asset.resource.data.locationId])
	metadata := {
		"resource": asset.name,
		"valid-locations": target_locations,
	}
}

#################
# Rule Utilities
#################

# Determine the overlap between locations under test and constraint
# By default (allowlist), we violate if there isn't overlap
#
check_location(mode,asset_location,target_locations) {
	mode == "allowlist"
	not asset_location in target_locations
}
check_location(mode,asset_location,target_locations) {
	mode == "denylist"
	asset_location in target_locations
}