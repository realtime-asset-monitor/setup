# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
package templates.gcp.CloudRunIngressAndIAMRoleV1

import data.validator.gcp.lib as lib
import future.keywords.in

############################################
# Find BigQuery Dataset Location Violations
############################################
deny[{
	"msg": message,
	"details": metadata,
}] {
	# trace(sprintf("authent: %v", [authent]))
	constraint := input.constraint

	asset := input.asset

	output := check_asset(asset, constraint)

	message := sprintf("Cloud Run Service %v has Ingress set to %v and IAM role set to %v has %v.", [output.name, output.ingress, output.member, output.role])
	metadata := {
		"resource": output.name,
		"ingress": output.ingress,
		"member": output.member,
		"role": output.role,
	}
}

check_asset(asset, constraint) = output {
	asset.asset_type == "run.googleapis.com/Service"
	iam_asset := get_from_fs("iamPolicy", asset.name)
	resource_asset := get_from_fs("resource", asset.name)

	output := {
		"name": iam_asset.mapValue.fields.iamPolicy.mapValue.fields.name.stringValue,
		"ingress": resource_asset.mapValue.fields.name.stringValue,
		"member": "",
		"role": "",
	}
}

check_asset(asset, constraint) = output {
	# Applies to CloudRunServices only
	asset.asset_type == "run.googleapis.com/Service"

	# Applies to resource content type only
	lib.get_constraint_params(constraint, params)
	constraint.metadata.annotations.assetType == "run.googleapis.com/Service"
	asset.resource != null

	ingress_rule := lib.get_default(params, "ingress", "internal")
	forbided_members := lib.get_default(params, "binding_members", ["allUsers", "allAuthenticatedUsers"])
	# trace(sprintf("ingress_rule: %v", [ingress_rule]))

	# Check on the resource
	# But not to Cloud Functions v2 (they use CloudRun)
	managed_by := lib.get_default(asset.resource.data.metadata.labels, "goog-managed-by", "NONE")

	# trace(sprintf("managed_by: %v", [managed_by])) 
	managed_by != "cloudfunctions"

	# Get the -short- name
	name := asset.resource.data.metadata.name

	# get the ingress setting
	ingress := lib.get_default(asset.resource.data.metadata.annotations, "run.googleapis.com/ingress", "all")

	# trace(sprintf("ingress: %v", [ingress]))
	# check the ingress setting against the constraint params
	ingress == ingress_rule

	# Call to Firestore for the iamPolicy
	iam_asset := get_from_fs("iamPolicy", asset.name)

	# trace(sprintf("iam_asset: %v", [iam_asset]))

	# Get the bindings 
	binding := iam_asset.mapValue.fields.iamPolicy.mapValue.fields.bindings.arrayValue.values[_]
	# trace(sprintf("binding: %v", [binding]))

	# Get the role
	role := binding.mapValue.fields.role.stringValue
	# trace(sprintf("role: %v", [role]))

	# Get the member
	member := binding.mapValue.fields.members.arrayValue.values[_].stringValue
	# trace(sprintf("member: %v", [member]))

	# Check the member against the forbided ones in the constraint
	member in forbided_members

	# Prepare Output
	output := {
		"name": name,
		"ingress": ingress,
		"member": member,
		"role": role,
	}
}

check_asset(asset, constraint) = output {
	# Applies to CloudRunServices only
	asset.asset_type == "run.googleapis.com/Service"

	# Apply to iamPolicy content type only
	lib.get_constraint_params(constraint, params)
	constraint.metadata.annotations.assetType == "IAM_POLICY"
	asset.iamPolicy != null

	ingress_rule := lib.get_default(params, "ingress", "internal")
	forbided_members := lib.get_default(params, "binding_members", ["allUsers", "allAuthenticatedUsers"])

	# Check on the iamPolicy
	# Get the bindings
	binding := asset.iamPolicy.bindings[_]

	# Get the role
	role := binding.role

	# Get the members
	member := binding.members[_]

	# Check the member against the forbided ones in the constraint
	member in forbided_members

	# Call to Firestore for the resource
	resource_asset := get_from_fs("resource", asset.name)
	# trace(sprintf("resource_asset: %v", [resource_asset]))

	# Check the resource
	# ignore Cloud Functions v2 (they use CloudRun)
	managed_by := lib.get_default(resource_asset.mapValue.fields.resource.mapValue.fields.data.mapValue.fields.metadata.mapValue.fields.labels.mapValue.fields, "goog-managed-by", {"stringValue": "NONE"})
	# trace(sprintf("managed_by: %v", [managed_by]))
	managed_by.stringValue != "cloudfunctions"

	# Get the -short- name
	name := resource_asset.mapValue.fields.resource.mapValue.fields.data.mapValue.fields.metadata.mapValue.fields.name.stringValue
	# trace(sprintf("name: %v", [name]))

	# get the ingress setting
	ingress := lib.get_default(resource_asset.mapValue.fields.resource.mapValue.fields.data.mapValue.fields.metadata.mapValue.fields.annotations.mapValue.fields, "run.googleapis.com/ingress", {"stringValue": "TOTO"})
	# trace(sprintf("ingress: %v", [ingress]))

	# check the ingress setting against the constraint param
	ingress.stringValue == ingress_rule

	output := {
		"name": name,
		"ingress": ingress,
		"member": member,
		"role": role,
	}
}

get_from_fs(type, name) = fs_doc {
	# TOKEN := opa.runtime().env.GCP_TOKEN
	# # trace(sprintf("TOKEN: %v", [TOKEN]))
	# FS_PROJECT := opa.runtime().env.MONITOR_PROJECT_ID
	token = get_instance_credentials_token()
	project_id = get_gcp_project_id()

	base_url := sprintf("https://firestore.googleapis.com/v1/projects/%v/databases/(default)/documents/assets/", [project_id])
	reverse_asset := replace(name, "/", "\\")
	doc := urlquery.encode(sprintf("%v", [reverse_asset]))
	url := build_url(base_url, doc, type)

	# trace(sprintf("url: %v", [url]))
	request := {
		"url": url,
		"method": "GET",
		"headers": {
			"Accept": "application/json",
			"Authorization": sprintf("Bearer %v", [token]),
		},
	}

	response := http.send(request)

	# trace(sprintf("response: %v", [response]))
	response.status_code == 200
	fs_doc := response.body.fields.asset
}

build_url(base_url, doc, type) = url {
	type == "resource"
	url := sprintf("%v%v?prettyPrint=false", [base_url, doc])
}

build_url(base_url, doc, type) = url {
	type == "iamPolicy"
	url := sprintf("%v%v_IAM_POLICY?prettyPrint=false", [base_url, doc])
}

get_gcp_project_id() := id {
	response := http.send({
		"method": "get",
		"cache": true,
		"url": "http://metadata.google.internal/computeMetadata/v1/project/project-id",
		"headers": {"Metadata-Flavor": "Google"},
	})
	response.status_code == 200
	id := response.raw_body
}

get_instance_credentials_token() := t {
	response := http.send({
		"method": "get",
		"cache": true,
		"url": "http://metadata.google.internal/computeMetadata/v1/instance/service-accounts/default/token",
		"headers": {"Metadata-Flavor": "Google"},
	})
	response.status_code == 200
	t := response.body.access_token
}
